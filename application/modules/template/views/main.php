<!-- header -->
<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $title; ?> </title>
        
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url()?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        
        <link href="<?= base_url()?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url()?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url()?>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url()?>assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= base_url()?>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url()?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="favicon.ico"/>
        <?php
            if(isset($css))
                foreach ($css as $value) {
                    echo '<link href="'.$value.'" rel="stylesheet" type="text/css">';
                }
            
        ?>
    <script src="<?= base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript">
    </script>
    <script src="<?= base_url()?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <script>
    jQuery(document).ready(function() {    
       Metronic.init(); // init metronic core componets
       Layout.init(); // init layout
       QuickSidebar.init(); // init quick sidebars
    });
    </script>
    <?php 
        if(isset($js))
            foreach ($js as $value) {
                echo '<script src="'.$value.'" type="text/javascript" charset="utf-8" async defer></script>';
            }
    ?>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">

    <!-- BEGIN HEADER -->
    <div class="page-header -i navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="index.html">
                <!-- <img src="<?= base_url(); ?>assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/> -->
                <!-- APLIKASI MOESTOPO -->
                </a>
                <div class="menu-toggler sidebar-toggler hide">
                </div>
                
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-default">
                        7 </span>
                        </a>
                    </li>
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<?= base_url(); ?>assets/admin/layout/img/avatar3_small.jpg"/>
                        <span class="username username-hide-on-mobile">
                        Nick </span>
                        <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="extra_profile.html">
                                <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="extra_lock.html">
                                <i class="icon-lock"></i> Lock Screen </a>
                            </li>
                            <li>
                                <a href="login.html">
                                <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->

    <div class="clearfix">
    </div>

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <li class="sidebar-toggler-wrapper">
                        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        <div class="sidebar-toggler">
                        </div>
                        <!-- END SIDEBAR TOGGLER BUTTON -->
                    </li>
                    
                    <li class="sidebar-search-wrapper">
                    
                        <!-- <form class="sidebar-search " action="extra_search.html" method="POST">
                            <a href="javascript:;" class="remove">
                            <i class="icon-close"></i>
                            </a>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                                </span>
                            </div>
                        </form> --><br>
                        <!-- END RESPONSIVE QUICK SEARCH FORM -->
                    </li>
                    <!-- <li class="start active open">
                        <a href="javascript:;">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="active">
                                <a href="index.html">
                                <i class="icon-bar-chart"></i>
                                Default Dashboard</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;">
                        <i class="icon-logout"></i>
                        <span class="title">Quick Sidebar</span>
                        <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="quick_sidebar_push_content.html">
                                Push Content</a>
                            </li>
                        </ul>
                    </li> -->
                    <li <?= ($menu=="jenis_pembayaran") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('jenis_pembayaran'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">JENIS PEMBAYARAN</span>
                        </a>
                    </li>
                    <li <?= ($menu=="pembayaran") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('pembayaran'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">PEMBAYARAN</span>
                        </a>
                    </li>
                    <li <?= ($menu=="semester") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('semester'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">SEMESTER</span>
                        </a>
                    </li>
                    <li <?= ($menu=="ruangan") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('ruangan'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">RUANGAN</span>
                        </a>
                    </li>
                    <li <?= ($menu=="periode") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('periode'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">PERIODE</span>
                        </a>
                    </li>
                    <li <?= ($menu=="dpjp") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('dpjp'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">DPJP</span>
                        </a>
                    </li>
                    <li <?= ($menu=="matakuliah") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('matakuliah'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">MATAKULIAH</span>
                        </a>
                    </li>
                    <li <?= ($menu=="lembar_khusus") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('lembar_khusus'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">LEMBAR KHUSUS</span>
                        </a>
                    </li>   
                    <li <?= ($menu=="kelompok") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('kelompok'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">KELOMPOK</span>
                        </a>
                    </li>  
                    <li <?= ($menu=="mahasiswa") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('mahasiswa'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">MAHASISWA</span>
                        </a>
                    </li>                    
                    <li <?= ($menu=="akademik") ? 'class="active"': ''; ?>>
                        <a href="<?= base_url('akademik'); ?>">
                        <i class="icon-logout"></i>
                        <span class="title">AKADEMIK</span>
                        </a>
                    </li>                    
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">                
                <!-- BEGIN PAGE HEADER-->
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="index.html">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Dashboard</a>
                        </li>
                    </ul>
                </div>
                <div class="row margin-top-20">
                </div>
                
                <?= $isi; ?>

            </div>
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER --> 

    <div class="page-footer">
        <div class="page-footer-inner">
             2014 &copy; Metronic by keenthemes.
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>

    
    </body>
</html>
