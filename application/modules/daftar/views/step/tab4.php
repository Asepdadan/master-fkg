
<h3 class="block">Konfirmasi Data</h3>

<h4 class="form-section">Profile</h4>
<div class="form-group">
    <label class="control-label col-md-3">nim:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="nim">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">nama lengkap:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="nama_lengkap">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Gender:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="gender">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Phone:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="phone">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Address:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="address">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">City/Town:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="city">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Country:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="country">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Remarks:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="remarks">
        </p>
    </div>
</div>
<h4 class="form-section">Billing</h4>
<div class="form-group">
    <label class="control-label col-md-3">Card Holder Name:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="card_name">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Card Number:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="card_number">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">CVC:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="card_cvc">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Expiration:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="card_expiry_date">
        </p>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Payment Options:</label>
    <div class="col-md-4">
        <p class="form-control-static" data-display="payment[]">
        </p>
    </div>
</div>