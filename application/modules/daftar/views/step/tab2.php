<div class="form-group">
    <label class="control-label col-md-2">Nim <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" class="form-control" name="nim"/>
        <span class="help-block">
        Nim Di universitas sebelemunya </span>
    </div>

    <label class="control-label col-md-2">Nama Lengkap <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" class="form-control" name="nama"/>
        <span class="help-block">
        nama lengkap </span>
    </div>
</div>
<div class="form-group">
   <label class="control-label col-md-2">Tanggal Lahir <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" class="form-control" name="tanggal_lahir"/>
        <span class="help-block">
        Tanggal Lahir </span>
    </div>

    <label class="control-label col-md-2">Tempat Lahir <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" class="form-control" name="tempat_lahir"/>
        <span class="help-block">
        tempat lahir </span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-2">Phone Number <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" class="form-control" name=""/>
        <span class="help-block">
        Provide your phone number </span>
    </div>

    <label class="control-label col-md-2">Jenis Kelamin <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <div class="radio-list">
            <label>
            <input type="radio" name="jenis_kelamin" value="1" data-title="LAKI LAKI"/>
            LAKI LAKI </label>
            <label>
            <input type="radio" name="jenis_kelamin" value="2" data-title="PEREMPUAN"/>
            PEREMPUAN </label>
        </div>
        <div id="form_gender_error">
            <span class="help-block">
            jenis kelamin </span>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-2">Negara</label>
    <div class="col-md-4">
        <select name="kewarganegaraan" id="negara" class="form-control">
            <option value="1">WNI (WARGA NEGARA INDONESIA)</option>
            <option value="2">WNA (WARGA NEGARA ASING)</option>
        </select>
    </div>

    <label class="control-label col-md-2">Agama *</label>
    <div class="col-md-4">
        <select name="jurusan" id="jurusan" class="form-control">
            <option value="1">Islam</option>
            <option value="2">Kristen</option>
            <option value="3">Katholik</option>
            <option value="4">Hindu</option>
            <option value="5">Budha</option>
            <option value="6">Konghucu</option>
            <option value="7">Lain - Lain</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-2">Alamat <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <textarea class="form-control" name="alamat" id="alamat"></textarea>
        <span class="help-block">
        alamat </span>
    </div>

    <label class="control-label col-md-2">Kelurahan <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <select id="kota" class="form-control select2">
            <option value="1">Bandung</option>
        </select>
        <span class="help-block">
        kelurahan </span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-2">Rt <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" class="form-control" name="rt" id="rt">
        <span class="help-block">
        Rukun Tetangga </span>
    </div>

    <label class="control-label col-md-2">Rw <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" class="form-control" name="rw" id="rw">
        <span class="help-block">
        Rukun Warga </span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-2">Status Perkawinan <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <div class="radio-list">
            <label>
            <input type="radio" name="status" value="1" data-title="Belum Nikah"/>
            Belum Nikah </label>
            <label>
            <input type="radio" name="status" value="2" data-title="Nikah"/>
            Nikah </label>
        </div>
        <div id="form_gender_error">
            <span class="help-block">
            Status Perkawinan </span>
        </div>
    </div>

    <label class="control-label col-md-2">Gelar <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" name="gelar" placeholder="gelar sebelumnya" class="form-control">
        <span class="help-block">
        Gelar </span>
    </div>
</div>


<div class="form-group">
    <label class="control-label col-md-2">Tahun Lulus <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" name="tahun_lulus" placeholder="tahun lulus sebelumnya" class="form-control">
        <span class="help-block">
        Tahun lulus </span>
    </div>

    <!-- <label class="control-label col-md-2">Gelar <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
        <input type="text" name="gelar" placeholder="gelar sebelumnya" class="form-control">
        <span class="help-block">
        Gelar </span>
    </div> -->
</div>

<div class="form-group">
    <!-- <label class="control-label col-md-2">Tahun Lulus <span class="required">
    * </span>
    </label> -->
    <button type="submit" class="btn btn-success col-md-offset-10 " id="save-button">SIMPAN</button>
</div>





