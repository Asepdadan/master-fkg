<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends MX_Controller {
    
    public function index()
    {
        $data['title'] = 'Daftar Mahasiswa';
        
        $this->load->view('daftar',$data);
    }
}