<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pembayaran extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_datatables_data()
    {
        $query = "SELECT a.*,b.nama_jenis
            FROM mst_pembayaran a 
                LEFT JOIN mst_jenis_pembayaran b ON a.id_jenis_pembayaran = b.id";

        return $this->db->query($query)->result_array();
    }

    public function create($data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->insert('mst_pembayaran', $data);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }

    public function update($id,$data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->update('mst_pembayaran', $data,['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }
    
    public function delete($id)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->delete('mst_pembayaran',['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }

    public function get_edit( $id )
    {
        $query = "SELECT a.*,b.nama_jenis
            FROM mst_pembayaran a 
                LEFT JOIN mst_jenis_pembayaran b ON a.id_jenis_pembayaran = b.id 
            WHERE a.id = ? ";

        return $this->db->query( $query, [$id] )->row_array();
    }

    public function get_jenis_pembayaran($search)
    {
        $query = "SELECT id, nama_jenis 
            FROM mst_jenis_pembayaran 
            WHERE nama_jenis LIKE '%{$search}%'";

        return $this->db->query($query)->result_array();
    }

}

/* End of file M_pembayaran.php */
/* Location: ./application/models/M_pembayaran.php */