<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"> 
<link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/datepicker3.css">

<script src="<?= base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/global/scripts/handlebars.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="<?= base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-mask/jquery.mask.min.js" type="text/javascript"></script>

<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?= $title; ?></span>
        </div>
    </div>
    <div class="portlet-body">
        <form id="form-info">
        </form>
    </div>
</div>

<script id="form-info-template" type="text/x-handlebars-template">
    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>MATAKULIAH</label>
            <select class="form-control" name="id_matakuliah" id="id_matakuliah"></select>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>NAMA KALAB</label>
            <input type="text" class="form-control" name="nama_kalab" value="{{EDIT.nama_kalab}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>NAMA LOKASI</label>
            <textarea class="form-control" name="nama_lokasi">{{EDIT.nama_lokasi}}</textarea>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>HARI KERJA</label>
            <textarea class="form-control" name="hari_kerja">{{EDIT.hari_kerja}}</textarea>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>TANGGAL MULAI</label>
            <input class="form-control" name="tanggal_mulai" value="{{EDIT.tanggal_mulai}}" id="tanggal_mulai">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>TANGGAL AKHIR</label>
            <input class="form-control" name="tanggal_akhir" value="{{EDIT.tanggal_akhir}}" id="tanggal_akhir">
        </div>
    </div>
    
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

    <div class="row margin-top-20">
        <div class="col-md-6">
            <button type="button" class="btn btn-success" id="save-button">SIMPAN</button>
            <button type="button" class="btn default" id="cancel-button">KEMBALI</button>
        </div>
    </div>
</script>

<script>
var myData = null;
var id = "<?= isset($id) ? $id : ''; ?>";
var template = Handlebars.compile($("#form-info-template").html());
var loaded = false;
$(document).ready(function() {
    var ajax_data = $.ajax({
        url: '<?= base_url('pengajuan/get_edit'); ?>/'+id,
        method  : 'POST',
        type    : 'json',
    }); 

    $.when(ajax_data).done(function(response_data) {
        data         = response_data;
        myData = data

        $("#form-info").empty()
        $("#form-info").append(template(myData)) 
        init(myData)
    });
 });
     
var init = function () {
    
    if(myData.hasOwnProperty('EDIT')){
        $("#id_matakuliah").empty()
        .append('<option value="'+ myData.EDIT.id_matakuliah +'">'+ myData.EDIT.nama_matakuliah +'</option>').val(myData.EDIT.id_matakuliah).trigger('change')
    } 


    $("#save-button").click(function(event) {
        Metronic.blockUI();

        var formData = new FormData();
        var formRawData = $('#form-info').serializeArray();
        var json_data = {data:{}};
        
        formRawData.forEach(function(element) {
            if(element.value!=""){
                formData.append(element.name, element.value);
                json_data[element.name] = element.value;
            }
        });

        formData.append('id', id);
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '<?= base_url(); ?>pengajuan/save', true);
        var onerror = function(event) {
            toastr.error("Error");
        }
        
        xhr.onload = function () {
            if (xhr.status === 200) {  
                response = JSON.parse(xhr.responseText);
                if(response.errorId == 0) {
                    toastr.success(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                       window.history.back()
                    },2000)                
                } else {
                    toastr.error(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                        window.history.back()
                    },2000)
                }
            }
        };

        xhr.send(formData);
        return false;    
    });

    $("#cancel-button").click(function(event) {
        window.history.back()
    }); 

    $("#id_matakuliah").select2({
        allowClear: true,
        width:"100%",
        placeholder: 'PILIH MATAKULIAH',
        ajax: {
            url: "<?= base_url(); ?>matakuliah/get_select2_matakuliah/",
            dataType: 'json',
            delay: 250,
            quietMillis: 50,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    type: params.type,
                };
            },
            processResults: function (data) {
                var rData = [];
                data.forEach(function(e) {
                rData.push({
                    'id': e['id'],
                    'text': e['nama'],
                });
            });
                return {
                    results: rData
                };
            },
        cache: true
        },
    })  

    $("#tanggal_mulai,#tanggal_akhir").datepicker({
        format : 'yyyy-mm-dd',
        autoclose : true
    })
}
</script>

