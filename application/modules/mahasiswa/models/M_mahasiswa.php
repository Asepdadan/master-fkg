<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_mahasiswa extends CI_Model {

    public $variable;

    public function __construct()
    {
        parent::__construct();
        
    }

    public function get_datatables_data()
    {
        $query = "SELECT *
            FROM mst_mahasiswa where is_delete = 0 order by id desc";

        return $this->db->query($query)->result_array();
    }

    public function get_edit( $id )
    {
        $query = "SELECT a.*,b.nama_semester,c.nama_periode
            FROM mst_mahasiswa a left join mst_semester b on a.semester = b.id
            left join mst_periode c on c.id = a.putaran
            WHERE a.id = ? ";

        $data = $this->db->query( $query, [$id] )->row_array();
        if($data['jenis_kelamin'] ==  1){
            $data['laki_laki'] = " checked";
        }else{
            $data['perempuan'] = " checked";
        }
        
        return $data;
    }


    public function create($data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->insert('mst_mahasiswa', $data);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }


    public function update( $id, $data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->update('mst_mahasiswa', $data,[ 'id' => $id ]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }

    public function delete($id)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->delete('mst_mahasiswa',['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }

    public function get_select2_mahasiswa($search)
    {
        $query = "select id,nim,nirm,nama from mst_mahasiswa where nama like '%{$search}%' or nim like '%{$search}%' or nirm like '%{$search}%'";

        return $this->db->query($query)->result_array();
    }

}

/* End of file M_mahasiswa.php */
/* Location: ./application/models/M_mahasiswa.php */