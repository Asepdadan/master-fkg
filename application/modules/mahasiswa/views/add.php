<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"> 
<link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/datepicker3.css">

<script src="<?= base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/global/scripts/handlebars.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="<?= base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-mask/jquery.mask.min.js" type="text/javascript"></script>

<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?= $title; ?></span>
        </div>
    </div>
    <div class="portlet-body">
        <form id="form-info">
        </form>
    </div>
</div>

<script id="form-info-template" type="text/x-handlebars-template">
    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>NIM</label>
            <input type="text" class="form-control" name="nim" id="nim" value="{{EDIT.nim}}">
        </div>

        <div class="col-md-6">
            <label>NIRM</label>
            <input type="text" class="form-control" name="nirm" id="nirm" value="{{EDIT.nirm}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>NAMA</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{EDIT.nama}}">
        </div>

        <div class="col-md-6">
            <label>TEMPAT LAHIR</label>
            <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" value="{{EDIT.tempat_lahir}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>TANGGAL LAHIR</label>
            <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value="{{EDIT.tanggal_lahir}}">
        </div>

        <div class="col-md-6">
            <label>JENIS KELAMIN</label>
            <div class="md-radio-list">
                <label><input type="radio" {{EDIT.laki_laki}} name="jenis_kelamin" id="laki_laki" value="1">LAKI LAKI</label>
                <label><input type="radio" {{EDIT.perempuan}} name="jenis_kelamin" id="perempuan" value="2">PEREMPUAN</label>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>KEWARGANEGARAAN</label>
            <input type="text" class="form-control" name="kewarganegaraan" id="kewarganegaraan" value="{{EDIT.kewarganegaraan}}">
        </div>

        <div class="col-md-6">
            <label>ALAMAT</label>
            <textarea name="alamat" class="form-control">{{EDIT.alamat}}</textarea>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>KELURAHAN</label>
            <input type="text" class="form-control" name="id_kelurahan" id="id_kelurahan" value="{{EDIT.id_kelurahan}}">
        </div>

        <div class="col-md-2">
            <label>RT</label>
            <input type="text" name="rt" class="form-control" value="{{EDIT.rt}}">
        </div>
        <div class="col-md-2">
            <label>RW</label>
            <input type="text" name="rw" class="form-control" value="{{EDIT.rw}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>AGAMA</label>
            <select class="form-control" id="agama" name="agama"> 
                <option value="islam">Islam</option>
                <option value="kristen">Kristen</option>
            </select>
        </div>

        <div class="col-md-6">
            <label>STATUS</label>
            <input type="text" name="status" class="form-control" value="{{EDIT.status}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>GELAR</label>
            <input type="text" class="form-control" id="gelar" name="gelar">
        </div>

        <div class="col-md-6">
            <label>TAHUN LULUS</label>
            <input type="text" name="tahun_lulus" class="form-control" value="{{EDIT.tahun_lulus}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>SEMESTER</label>
            <select id="semester" class="form-control" name="semester"></select>
        </div>

        <div class="col-md-6">
            <label>PERIODE</label>
            <select id="periode" class="form-control" name="putaran"></select>
        </div>
    </div>
    
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

    <div class="row margin-top-20">
        <div class="col-md-6">
            <button type="button" class="btn btn-success" id="save-button">SIMPAN</button>
            <button type="button" class="btn default" id="cancel-button">KEMBALI</button>
        </div>
    </div>
</script>

<script>
var myData = null;
var id = "<?= isset($id) ? $id : ''; ?>";
var template = Handlebars.compile($("#form-info-template").html());
var loaded = false;
$(document).ready(function() {
    var ajax_data = $.ajax({
        url: '<?= base_url('mahasiswa/get_edit'); ?>/'+id,
        method  : 'POST',
        type    : 'json',
    }); 

    $.when(ajax_data).done(function(response_data) {
        data         = response_data;
        myData = data

        $("#form-info").empty()
        $("#form-info").append(template(myData)) 
        init(myData)
        $("input[type='radio']").uniform();
    });
 });
     
var init = function () {
    
    if(myData.hasOwnProperty('EDIT')){
        $("#semester")
        .empty().
        append('<option value="'+ myData.EDIT.semester +'">'+ myData.EDIT.nama_semester +'</option>').val(myData.EDIT.semester).trigger('change')

        $("#periode")
        .empty().
        append('<option value="'+ myData.EDIT.putaran +'">'+ myData.EDIT.nama_periode +'</option>').val(myData.EDIT.putaran).trigger('change')
    } 

    $("#save-button").click(function(event) {
        Metronic.blockUI();

        var formData = new FormData();
        var formRawData = $('#form-info').serializeArray();
        var json_data = {data:{}};
        
        formRawData.forEach(function(element) {
            if(element.value!=""){
                formData.append(element.name, element.value);
                json_data[element.name] = element.value;
            }
        });

        formData.append('id', id);
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '<?= base_url(); ?>mahasiswa/save', true);
        var onerror = function(event) {
            toastr.error("Error");
        }
        
        xhr.onload = function () {
            if (xhr.status === 200) {  
                response = JSON.parse(xhr.responseText);
                if(response.errorId == 0) {
                    toastr.success(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                       window.history.back()
                    },2000)                
                } else {
                    toastr.error(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                        window.history.back()
                    },2000)
                }
            }
        };

        xhr.send(formData);
        return false;    
    });

    $("#cancel-button").click(function(event) {
        window.history.back()
    });    

    $("#agama").select2({
        placeholder : "PILIH AGAMA"
    });

    $("#semester").select2({
        allowClear: true,
        width:"100%",
        placeholder: 'PILIH SEMESTER',
        ajax: {
            url: "<?= base_url(); ?>semester/get_select2_semester/",
            dataType: 'json',
            delay: 250,
            quietMillis: 50,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    type: params.type,
                };
            },
            processResults: function (data) {
                var rData = [];
                data.forEach(function(e) {
                rData.push({
                    'id': e['id'],
                    'text': e['nama_semester'],
                });
            });
                return {
                    results: rData
                };
            },
        cache: true
        },
    })

    $("#periode").select2({
        allowClear: true,
        width:"100%",
        placeholder: 'PILIH PERIODE',
        ajax: {
            url: "<?= base_url(); ?>periode/get_select2_periode/",
            dataType: 'json',
            delay: 250,
            quietMillis: 50,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    type: params.type,
                };
            },
            processResults: function (data) {
                var rData = [];
                data.forEach(function(e) {
                rData.push({
                    'id': e['id'],
                    'text': e['nama_periode'],
                });
            });
                return {
                    results: rData
                };
            },
        cache: true
        },
    })

    $("#tanggal_lahir").datepicker({
        autoclose : true,
        format : "yyyy-mm-dd"
    })
}
</script>

