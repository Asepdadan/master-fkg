<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_pembayaran extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_jenis_pembayaran');
    }

    public function index()
    {
        $data['title'] = ucfirst('jenis pembayaran');
        $data['menu'] = "jenis_pembayaran";

        $this->render_page('index',$data);
    }

    public function get_datatables_data()
    {
        $result = $this->M_jenis_pembayaran->get_datatables_data();
        
        header("Content-type: application/json");
        echo json_encode($result);
    }

    public function posts()
    {
        $list = $this->M_jenis_pembayaran->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = "";
            $row[] = $field['id'];
            $row[] = $field['nama_jenis'];
            $row[] = '<a href="javascript:void(0)" class="btn btn-danger btn-hapus-row" data-row="'.$field['id'].'" data-toggle="confirmation"><i class="fa fa-trash"></i></i></a>';

            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_jenis_pembayaran->count_all(),
            "recordsFiltered" => $this->M_jenis_pembayaran->count_filtered(),
            "data" => $data,
        );

        header('Access-Control-Allow-Origin: *');
        echo json_encode($output);
    }

    public function save()
    {
        $data = $this->input->post();

        unset($data['ci_csrf_token']);

//        print_r($data);die;

        if( empty($data['id']) ){

            $sukses = $this->M_jenis_pembayaran->create($data);
        }else{

            $sukses = $this->M_jenis_pembayaran->update($data['id'],$data);
        }

        $result = [
            "errorId" => $sukses['success'] ? 0 : 1,
            "message" => $sukses['success'] ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        header('Content-type: application/json');
        echo json_encode($result);
    }

    public function delete()
    {
        $data = $this->input->post();

        $sukses = $this->M_jenis_pembayaran->delete(base64_decode($data['id']));

        $result = [
            "errorId" => $sukses['success'] ? 0 : 1,
            "message" => $sukses['success'] ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        header('Content-type: application/json');
        echo json_encode($result);   
    }

}

/* End of file Jenis_pembayaran.php */
/* Location: ./application/controllers/Jenis_pembayaran.php */