<link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css">
<style type="text/css">
    td.details-control {
        background: url('https://datatables.net/examples//resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples//resources/details_close.png') no-repeat center center;
    }
</style>

<script src="<?= base_url(); ?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript" charset="utf-8" async defer></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript" charset="utf-8" async defer></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript" charset="utf-8" async defer></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript" charset="utf-8" async defer></script>
<script src="<?= base_url();?>assets/global/scripts/handlebars.js" type="text/javascript" charset="utf-8" async defer></script>
<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?= $title; ?></span>
        </div>
        <ul class="nav nav-tabs">
            <li class="active">
                <a class="tabs" href="#tab_info" data-toggle="tab">
                    Info
                </a>
            </li>
        </ul>
    </div>
    <div class="portlet-body">
        <!-- <div class="table-responsive"> -->
            <table class="table table-hover table-responsive table-bordered" id="tabel-jenis-pembayaran">
                <thead>
                    <tr>
                        <th><i class="fa fa-eye"></i></th>
                        <th>NO</th>
                        <th>JENIS PEMBAYARAN</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
            </table>
        <!-- </div> -->

    <a href="#modal-jenis-pembayaran" class="btn btn-primary" data-toggle="modal">Tambah</a>

    </div>
</div>

<script id="edit-template" type="text/x-handlebars-template">
    <form id="form-info">
    <table class="table table-condensed table-hover">
        <tbody>
            <tr>
                <td>
                    <input type="text" class="form-control" name="nama_jenis" value="{{2}}">
                    <input type="hidden" class="form-control" name="id" value="{{1}}">
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td width="15%"><button class="btn btn-success" type="button" onclick="save()">Simpan</button></td>
            </tr>
        </tfoot>
    </table>
    </form>
</script>

<div class="modal fade" id="modal-jenis-pembayaran" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Form Tambah</h4>
            </div>
            <form id="form-info">
            <div class="modal-body">
                <div class="row margin-top-10">
                    <div class="col-md-6">
                        <label>Jenis Pembayaran</label>
                        <input type="text" class="form-control" name="nama_jenis">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue" id="save-button">Save changes</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    var tabel = ""
 $(document).ready(function() {
    //datatables
    var template = Handlebars.compile($("#edit-template").html());
    tabel = $('#tabel-jenis-pembayaran').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "ordering": false,
        "ajax": {
            "url": "<?php echo base_url('jenis_pembayaran/posts')?>",
            "type": "POST",
            "dataType": 'json',
            "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
        },
       "columnDefs": [
            { 
                "targets": 0 , 
                "orderable": false, 
                "className": "text-center details-control",
                "width": "5%"
            },
            {
                  "targets": 1,
                  "className": "text-center",
                  "visible": false,
                  "width": "5%"
            },{
                  "targets": 2,
                  "className": "text-center",
                  "width": "80%"
            },
            {
                  "targets": 3,
                  "className": "text-center",
                  "searchable": false,
                  "width": "15%"
            }
        ],
    });

    $('#tabel-jenis-pembayaran tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tabel.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( template(row.data()) ).show();
            tr.addClass('shown');
        }
    });
    
    $('#tabel-jenis-pembayaran').on('click', '.btn-hapus-row', function(event) {
       var row = $(this).data("row")
       if (confirm('Are you sure you want to save this thing into the database?')) {
            // Save it!
            hapus_row(row)
        } else {
            // Do nothing!
        }

    });
    
    $("#modal-jenis-pembayaran").on('show.bs.modal', function(event) {
        $("input").val(" ")
    });

    $("#save-button").click(function(event) {
        Metronic.blockUI();

        var formData = new FormData();

        var formRawData = $('#form-info').serializeArray();
        var json_data = {data:{}};
        
        formRawData.forEach(function(element) {
            if(element.value!=""){
                formData.append(element.name, element.value);
                json_data[element.name] = element.value;
            }
        });

        formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>')

        var xhr = new XMLHttpRequest();

        xhr.open('POST', 'jenis_pembayaran/save', true);

        var onerror = function(event) {
            toastr.error("Error");
        }

        xhr.onload = function () {
            
            if (xhr.status === 200) {
                
                response = JSON.parse(xhr.responseText);

                if(response.errorId == 0) {
                    toastr.success(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                        tabel.ajax.reload()
                        $("#modal-jenis-pembayaran").modal("hide")
                    },2000)                
                } else {
                    toastr.error(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                        tabel.ajax.reload()
                        $("#modal-jenis-pembayaran").modal("hide")
                    },2000)
                }
            }
        };

        xhr.send(formData);
        return false;    
    });


 });

 function hapus_row(row) {
    //alert(row)
    $.ajax({
        url: '<?= base_url('jenis_pembayaran/delete'); ?>',
        type: 'POST',
        dataType: 'json',
        data: {id: btoa(row),'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
    })
    .done(function(response) {
        if(response.errorId == 0) {
            toastr.success(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)                
        } else {
            toastr.error(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)
        }
    })    
 }
 function save ( id ) {   
    Metronic.blockUI();

    var formData = new FormData();

    var formRawData = $('#form-info').serializeArray();
    var json_data = {data:{}};
    
    formRawData.forEach(function(element) {
        if(element.value!=""){
            formData.append(element.name, element.value);
            json_data[element.name] = element.value;
        }
    });

    formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>')
    //formData.append('id',id)

    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'jenis_pembayaran/save', true);

    var onerror = function(event) {
        toastr.error("Error");
    }

    xhr.onload = function () {
        

        if (xhr.status === 200) {
            
            response = JSON.parse(xhr.responseText);

            if(response.errorId == 0) {
                toastr.success(response.message)
                setTimeout(function(){
                    Metronic.unblockUI();
                    tabel.ajax.reload()
                },2000)                
            } else {
                toastr.error(response.message)
                setTimeout(function(){
                    Metronic.unblockUI();
                    tabel.ajax.reload()
                },2000)
            }
        }
    };

    xhr.send(formData);
    return false;
}
</script>
