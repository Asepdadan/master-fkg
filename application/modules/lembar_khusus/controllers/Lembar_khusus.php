<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lembar_khusus extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_lembar_khusus');
    }

    public function index()
    {
        $data['title'] = ucfirst('lembar khusus');
        $data['menu'] = "dpjp";
        $this->render_page('index',$data);    
    }

    public function add( $id = "" )
    {
        $data['title'] = ucfirst('form lembar khusus');
        $data['menu'] = "dpjp";
        $data['id'] = $id;

        $this->render_page('add',$data);    
    }

    public function get_datatables_data()
    {
        $data = $this->M_lembar_khusus->get_datatables_data();
        $array = array("data" => $data);

        header('Content-Type: application/json');
        echo json_encode($array);
    }

    public function get_edit( $id = "" )
    {
        $data = [];

        if( ! empty($id) )
            $data['EDIT'] = $this->M_lembar_khusus->get_edit( $id );


        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function save()
    {
        $data = $this->input->post();
        
        unset($data['ci_csrf_token']);        

        if(empty($data['id'])){

            $success = $this->M_lembar_khusus->create($data);
        }else{

            $success = $this->M_lembar_khusus->update($data['id'],$data);
        }

        $result = [
            "errorId" => $success['success'] ? 0 : 1,
            "message" => $success['success'] ? "Data berhasil disimpan" : "Data gagal disimpan",
        ];

        header("Content-type: application/json");
        echo json_encode($result);
    }

    public function delete()
    {
        $data = $this->input->post();

        $sukses = $this->M_lembar_khusus->delete(base64_decode($data['id']));

        $result = [
            "errorId" => $sukses['success'] ? 0 : 1,
            "message" => $sukses['success'] ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        header('Content-type: application/json');
        echo json_encode($result);   
    }

}