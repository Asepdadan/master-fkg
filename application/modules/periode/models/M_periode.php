<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_periode extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_datatables_data()
    {
        $query = "SELECT a.id,a.tanggal_mulai,a.tanggal_akhir,a.periode,a.tahun_angkatan,b.id as id_master_matakuliah,b.nama as nama_matakuliah
            FROM akd_periode a left join mst_matakuliah b on a.id_master_matakuliah = b.id order by a.id desc";

        return $this->db->query($query)->result_array();
    }

    public function get_edit( $id )
    {
        $query = "SELECT a.id,a.tanggal_mulai,a.tanggal_akhir,a.periode,a.tahun_angkatan,b.id as id_master_matakuliah,b.nama as nama_matakuliah
            FROM akd_periode a left join mst_matakuliah b on a.id_master_matakuliah = b.id where a.id = ? ";
        $data = $this->db->query( $query, [$id] )->row_array();

        if($data['periode'] == 1){
            $data['periode1'] = " selected";
        }else{
            $data['periode2'] = " selected";
        }

        return $data;
    }


    public function create($data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->insert('akd_periode', $data);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }


    public function update( $id, $data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->update('akd_periode', $data,[ 'id' => $id ]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }

    public function delete($id)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->delete('akd_periode',['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }

    public function get_select2_periode($search)
    {
        $query = "select id,nama_periode from akd_periode where nama_periode like '%{$search}%'";

        return $this->db->query($query)->result_array();
    }

}

/* End of file M_periode.php */
/* Location: ./application/models/M_periode.php */