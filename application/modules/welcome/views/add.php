
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase">Usulan kegiatan seminar</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
                <li class="pull-right">
                    <a href="#tab_15_1" data-toggle="tab" aria-expanded="false">
                    Section 1 </a>
                </li>
                <li class="active pull-right">
                    <a href="#tab_15_2" data-toggle="tab" aria-expanded="true">
                    Section 2 </a>
                </li>
                <li class="pull-right">
                    <a href="#tab_15_3" data-toggle="tab" aria-expanded="false">
                    Section 3 </a>
                </li>
                <!-- <li class="dropdown pull-right tabdrop active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-ellipsis-v"></i>&nbsp;<i class="fa fa-angle-down"></i> <b class=""></b></a>
                    <ul class="dropdown-menu">
                        <li class="active">
                            <a href="#tab15_5" data-toggle="tab">Section 5</a>
                        </li><li>
                            <a href="#tab15_5" data-toggle="tab">Section 6</a>
                        </li><li>
                            <a href="#tab17" data-toggle="tab">Section 7</a>
                        </li><li>
                            <a href="#tab18" data-toggle="tab">Section 8</a>
                        </li>
                    </ul>
                </li> -->
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_15_1">
                    <p>
                         I'm in Section 1.
                    </p>
                    <p>
                         Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.
                    </p>
                </div>
                <div class="tab-pane active" id="tab_15_2">
                    <p>
                         Howdy, I'm in Section 2.
                    </p>
                    <p>
                         Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
                    </p>
                    <p>
                        <a class="btn green" href="ui_tabs_accordions_navs.html#tab_15_2" target="_blank">
                        Activate this tab via URL </a>
                    </p>
                </div>
                <div class="tab-pane" id="tab_15_3">
                    <p>
                         Howdy, I'm in Section 3.
                    </p>
                    <p>
                         Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
                    </p>
                    <p>
                        <a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_15_3" target="_blank">
                        Activate this tab via URL </a>
                    </p>
                </div>
                <div class="tab-pane" id="tab15_5">
                    <p>
                         Howdy, I'm in Section 5.
                    </p>
                    <p>
                         Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
                    </p>
                    <p>
                        <a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_15_3" target="_blank">
                        Activate this tab via URL </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>