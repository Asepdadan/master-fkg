<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends MX_Controller {

    function __construct()
    {
        $this->load->library('parser');
        $this->murl = '/modules/'.$this->uri->segment(1).'/';
    }
    
    // public function index()
    // {
    //     $this->load->view('welcome_message');
    // }

    public function parser()
    {
        $data['title'] = 'Template Parser';
        $data['css'] = [
            base_url().'assets/global/plugins/bootstrap-datepicker/css/datepicker3.css',
            base_url().'assets/global/plugins/bootstrap-select/bootstrap-select.css',
        ];
        $data['js'] = "";
        
        $this->render_page('test',$data);
    }

    public function hello()
    {
//        echo "hello";
        $data['murl'] = $this->murl;
        echo modules::run("login/hello","Hello","Asep","Dadan");
    }

    public function template()
    {
        $data = array(
            'blog_title' => 'My Blog Title',
            'blog_heading' => 'My Blog Heading',
            'blog_entries' => array(
                array('title' => '<h3>Hello bos </h3>', 'body' => 'Body 1'),
                array('title' => 'Title 2', 'body' => 'Body 2'),
                array('title' => 'Title 3', 'body' => 'Body 3'),
                array('title' => 'Title 4', 'body' => 'Body 4'),
                array('title' => 'Title 5', 'body' => 'Body 5')
            ),
            'blog' => $this->load->view('head')
        );

        $this->parser->parse('blog_template', $data);
    }

    public function sqlserver()
    {
        $otherdb = $this->load->database('default1', TRUE);

        print_r($otherdb);die;
    }

}