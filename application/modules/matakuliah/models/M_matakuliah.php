<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_matakuliah extends CI_Model {

    public $variable;

    public function __construct()
    {
        parent::__construct();
        
    }

    public function get_datatables_data()
    {
        $query = "SELECT *
            FROM mst_matakuliah where is_delete = 0 order by id desc";

        return $this->db->query($query)->result_array();
    }

    public function get_edit( $id )
    {
        $query = "SELECT *
            FROM mst_matakuliah
            WHERE id = ? ";

        return $this->db->query( $query, [$id] )->row_array();
    }


    public function create($data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->insert('mst_matakuliah', $data);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }


    public function update( $id, $data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->update('mst_matakuliah', $data,[ 'id' => $id ]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }

    public function delete($id)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->delete('mst_matakuliah',['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }

    public function get_data_matakuliah()
    {
        $query = "select id,kode,nama,status from mst_matakuliah";

        return $this->db->query($query)->result_array();
    }

    public function get_select2_matakuliah($search)
    {
        $query = "select id,kode,nama,status from mst_matakuliah where nama like '%{$search}%'";

        return $this->db->query($query)->result_array();
    }

}

/* End of file M_matakuliah.php */
/* Location: ./application/models/M_matakuliah.php */