<link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css">

<script src="<?= base_url(); ?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/global/scripts/handlebars.js" type="text/javascript"></script>


<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?= $title; ?></span>
        </div>
    </div>
    <div class="portlet-body">
        <!-- <div class="table-responsive"> -->
            <table class="table table-hover table-responsive table-bordered" id="tabel-dpjp">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NAMA DPJP</th>
                        <th>TEMPAT LAHIR</th>
                        <th>TANGGAL LAHIR</th>
                        <th>JENIS DPJP</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        <!-- </div> -->
        <div class="row margin-top-20">
            <a href="<?= base_url('dpjp/add'); ?>" class="btn btn-primary">Tambah</a>
        </div>
    </div>
</div>

<script>
var tabel = "";
$(function () {
    tabel = $('#tabel-dpjp').DataTable({
        "processing": true,
        "ajax": "<?= base_url('dpjp/get_datatables_data'); ?>",
        "deferRender": true,
        "aLengthMenu": [[5, 10, 50, -1],[ 5, 10, 50, "All"]],
        "columns": [
            { "data": "id" },
            { "data": "nama" },
            { "data": "tempat_lahir" },
            { "data": "tanggal_lahir" },
            //{ "data": "jenis_dpjp" },
            { "render": function ( data, type, row ) {
                    if(row.jenis_dpjp == "1"){
                        return '<span class="label label-success">Dokter</span>'
                    }
                },
                "targets": 4
            },
            { "render": function ( data, type, row ) {
                var html  = "<button class='btn btn-warning btnedit' data-id='"+ row.id +"' type='button'><i class='fa fa-edit' onclick='edit_data(\""+ row.id +"\")'></i></button> "
                    html += " <button data-row='"+ row.id +"' class='btn btn-danger btn-hapus-row' type='button' id='hapus'><i class='fa fa-trash'></i></button>"

                return html
                },
                "targets": 5
            },
        ],
    });

});

$(document).ready(function() {
    
    $('#tabel-dpjp').on('click', '.btn-hapus-row', function(event) {
       var row = $(this).data("row")
       if (confirm('Are you sure you want to save this thing into the database?')) {
            hapus_row(row)
        } else {
            
        }
    });

    $("#tabel-dpjp").on('click', '.btnedit', function(event) {
        document.location = '<?php base_url(); ?>dpjp/add/'+$(this).data('id')+'/'
    });

 });


function hapus_row(row) {
    //alert(row)
    $.ajax({
        url: '<?= base_url('dpjp/delete'); ?>',
        type: 'POST',
        dataType: 'json',
        data: {id: btoa(row),'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
    })
    .done(function(response) {
        if(response.errorId == 0) {
            toastr.success(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)                
        } else {
            toastr.error(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)
        }
    })    
 }
</script>

