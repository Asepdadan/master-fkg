<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"> 
<link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/datepicker3.css">

<script src="<?= base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/global/scripts/handlebars.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="<?= base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script>
    Handlebars.registerHelper('if_same', function(a, b, options) {
    
        if(a == b) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
</script>

<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?= $title; ?></span>
        </div>
    </div>
    <div class="portlet-body">
        <form id="form-info">
        </form>
    </div>
</div>

<script id="form-info-template" type="text/x-handlebars-template">
    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>NAMA DPJP</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{EDIT.nama}}">
        </div>
        <div class="col-md-6">
            <label>TEMPAT LAHIR</label>
            <input type="text" class="form-control" name="tempat_lahir" value="{{EDIT.tempat_lahir}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>TANGGAL LAHIR</label>
            <input type="text" class="form-control" name="tanggal_lahir" 
            value=" {{EDIT.tanggal_lahir}}"
            id="tanggal_lahir">
        </div>

        <div class="col-md-6">
            <label>JENIS KELAMIN</label>
            <div class="md-list-radio">
                <label><input type="radio" {{#if_same EDIT.jenis_kelamin '1'}} checked {{/if_same}} class="form-control" name="jenis_kelamin" value="1">PRIA</label>
                <label><input type="radio" {{#if_same EDIT.jenis_kelamin '2'}} checked {{/if_same}} class="form-control" name="jenis_kelamin" value="2">WANITA</label>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>KEWARGANEGARAAN</label>
            <select class="form-control select2" name="kewarganegaraan" id="kewarganegaraan">
                <option></option>
                <option value="1">INDONESIA</option>
            </select>
        </div>

        <div class="col-md-6">
            <label>KELURAHAN</label>
            <select name="id_kelurahan" class="form-control select2" id="id_kelurahan">
                <option></option>
                <option value="1">Cikembulan</option>
            </select>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <label>ALAMAT</label>
            <textarea name="alamat" class="form-control">{{EDIT.alamat}}</textarea>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>RW</label>
            <input type="text" name="rw" class="form-control" value="{{EDIT.rw}}">
        </div>

        <div class="col-md-6">
            <label>RT</label>
            <input type="text" name="rt" class="form-control" value="{{EDIT.rt}}">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>AGAMA</label>
            <select name="agama" class="form-control select2" id="agama">
                <option></option>
                <option value="1">ISLAM</option>
                <option value="2">KRISTER</option>
                <option value="3">BUDAH</option>
                <option value="4">KATOLIK</option>
                <option value="5">PROTESTAN</option>
                <option value="6">KONGHUCU</option>
            </select>
        </div>

        <div class="col-md-6">
            <label>JENIS DPJP</label>
            <select name="jenis_dpjp" class="form-control select2" id="jenis_dpjp">
                <option></option>
                <option value="1">DOKTER</option>
            </select>
        </div>
    </div>
    
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

    <div class="row margin-top-20">
        <div class="col-md-6">
            <button type="button" class="btn btn-success" id="save-button">SIMPAN</button>
            <button type="button" class="btn default" id="cancel-button">KEMBALI</button>
        </div>
    </div>
</script>

<script>
var myData = null;
var id = "<?= isset($id) ? $id : ''; ?>";
var template = Handlebars.compile($("#form-info-template").html());
var loaded = false;
$(document).ready(function() {
    var ajax_data = $.ajax({
        url: '<?= base_url('dpjp/get_edit'); ?>/'+id,
        method  : 'POST',
        type    : 'json',
    }); 

    $.when(ajax_data).done(function(response_data) {
        data         = response_data;
        myData = data

        $("#form-info").empty()
        $("#form-info").append(template(myData)) 
        init(myData)
    });
 });
     
var init = function () {
    
    $("input[type='radio']").uniform()

    if(myData.hasOwnProperty('EDIT')){
        $("#kewarganegaraan").val(myData.EDIT.kewarganegaraan).trigger('change')
        $("#agama").val(myData.EDIT.agama).trigger('change')
        $("#jenis_dpjp").val(myData.EDIT.jenis_dpjp).trigger('change')
        $("#id_kelurahan").val(myData.EDIT.id_kelurahan).trigger('change')
    } 

    $("#save-button").click(function(event) {
        Metronic.blockUI();

        var formData = new FormData();
        var formRawData = $('#form-info').serializeArray();
        var json_data = {data:{}};
        
        formRawData.forEach(function(element) {
            if(element.value!=""){
                formData.append(element.name, element.value);
                json_data[element.name] = element.value;
            }
        });

        formData.append('id', id);
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '<?= base_url(); ?>dpjp/save', true);
        var onerror = function(event) {
            toastr.error("Error");
        }
        
        xhr.onload = function () {
            if (xhr.status === 200) {  
                response = JSON.parse(xhr.responseText);
                if(response.errorId == 0) {
                    toastr.success(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                       window.history.back()
                    },2000)                
                } else {
                    toastr.error(response.message)
                    setTimeout(function(){
                        Metronic.unblockUI();
                        window.history.back()
                    },2000)
                }
            }
        };

        xhr.send(formData);
        return false;    
    });

    $("#cancel-button").click(function(event) {
        window.history.back()
    });    

    $(".select2").select2({
        placeholder : "PILIH"
    })

    $('#tanggal_lahir').datepicker({
        rtl: Metronic.isRTL(),
        orientation: "top",
        autoclose: true,
        format : 'yyyy-mm-dd',
    });
}
</script>

