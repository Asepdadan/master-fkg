<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dpjp extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_datatables_data()
    {
        $query = "SELECT id,nama,tempat_lahir,tanggal_lahir,jenis_dpjp
            FROM mst_dpjp order by id desc";

        return $this->db->query($query)->result_array();
    }

    public function get_edit( $id )
    {
        $query = "SELECT *
            FROM mst_dpjp
            WHERE id = ? ";

        return $this->db->query( $query, [$id] )->row_array();
    }


    public function create($data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->insert('mst_dpjp', $data);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }


    public function update( $id, $data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->update('mst_dpjp', $data,[ 'id' => $id ]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }

    public function delete($id)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->delete('mst_dpjp',['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }

    public function get_select2_dpjp($search)
    {
        $query = "select id,nama from mst_dpjp where is_delete = 0 and nama like '%{$search}%'";

        return $this->db->query($query)->result_array();
    }

}

/* End of file M_dpjp.php */
/* Location: ./application/models/M_dpjp.php */