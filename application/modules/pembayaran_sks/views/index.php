<link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"> 


<script src="<?= base_url(); ?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/global/scripts/handlebars.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?= $title; ?></span>
        </div>
    </div>
    <div class="portlet-body">
        <!-- <div class="table-responsive"> -->
            <table class="table table-hover table-responsive table-bordered" id="tabel-pembayaran">
                <thead>
                    <tr>
                        <!-- <th><i class="fa fa-eye"></i></th> -->
                        <th>NO</th>
                        <th>NAMA JENIS PEMBAYARAN</th>
                        <th>TAHUN ANGKATAN</th>
                        <th>HARGA</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
            </table>
        <!-- </div> -->
        <div class="row margin-top-20">
            <a href="<?= base_url('pembayaran/add'); ?>" class="btn btn-primary">Tambah</a>
        </div>
    </div>
</div>

<script id="form-info-template" type="text/x-handlebars-template">
    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>JENIS PEMBAYARAN</label>
            <select class="form-control" id="jenis_pembayaran" name="id_jenis_pembayaran" >
            </select>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>TAHUN ANGKATAN</label>
            <input type="text" class="form-control" name="tahun_angkatan">
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <label>HARGA</label>
            <input type="text" class="form-control" name="harga">
        </div>
    </div>
</script>


<script>
var tabel = "";
$(function () {
    tabel = $('#tabel-pembayaran').DataTable({
        "processing": true,
        "ajax": "<?= base_url('pembayaran/get_datatables_data'); ?>",
        "deferRender": true,
        "aLengthMenu": [[5, 10, 50, -1],[ 5, 10, 50, "All"]],
        "columns": [
            { "data": "id" },
            { "data": "nama_jenis" },
            { "data": "tahun_angkatan" },
            { "render": function function_name( data, type, row ) {
                
                return "Rp. "+row.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },
                "targets": 4
            },
            { "render": function ( data, type, row ) {
                var html  = "<button class='btn btn-warning btnedit' data-id='"+ row.id +"' type='button'><i class='fa fa-edit' onclick='edit_data(\""+ row.id +"\")'></i></button> "
                    html += " <button onclick='hapus_row()' data-row='"+ row.id +"' class='btn btn-danger btn-hapus-row' type='button' id='hapus'><i class='fa fa-trash'></i></button>"

                return html
                },
                "targets": 6
            },
        ],
    });

});

$(document).ready(function() {
    
    var template = Handlebars.compile($("#form-info-template").html());

    $('#tabel-pembayaran').on('click', '.btn-hapus-row', function(event) {
       var row = $(this).data("row")
       if (confirm('Are you sure you want to save this thing into the database?')) {
            hapus_row(row)
        } else {
            
        }
    });

    $("#tabel-pembayaran").on('click', '.btnedit', function(event) {
        document.location = '<?php base_url(); ?>pembayaran/add/'+$(this).data('id')+'/'
    });

 });


function hapus_row(row) {
    //alert(row)
    $.ajax({
        url: '<?= base_url('pembayaran/delete'); ?>',
        type: 'POST',
        dataType: 'json',
        data: {id: btoa(row),'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
    })
    .done(function(response) {
        if(response.errorId == 0) {
            toastr.success(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)                
        } else {
            toastr.error(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)
        }
    })    
 }
</script>

