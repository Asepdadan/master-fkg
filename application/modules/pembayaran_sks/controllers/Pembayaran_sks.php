<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran_sks extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_pembayaran');
    }

    public function index()
    {
        $data['title'] = ucfirst('pembayaran');
        $data['menu'] = "dpjp";
        $this->render_page('index',$data);    
    }

    public function add( $id = "" )
    {
        $data['title'] = ucfirst('form pembayaran');
        $data['menu'] = "dpjp";
        $data['id'] = $id;

        $this->render_page('add',$data);    
    }

    public function save()
    {
        $data = $this->input->post();
        
        unset($data['ci_csrf_token']);
        $data['harga'] = str_replace(".", "", $data['harga']);

        if(empty($data['id'])){

            $success = $this->M_pembayaran->create($data);
        }else{

            $success = $this->M_pembayaran->update($data['id'],$data);
        }

        $result = [
            "errorId" => $success['success'] ? 0 : 1,
            "message" => $success['success'] ? "Data berhasil disimpan" : "Data gagal disimpan",
        ];

        header("Content-type: application/json");
        echo json_encode($result);
    }

    public function get_edit( $id = "" )
    {
        $data = [];

        if( ! empty($id) )
            $data['EDIT'] = $this->M_pembayaran->get_edit( $id );


        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function get_datatables_data()
    {
        $data = $this->M_pembayaran->get_datatables_data();
        $array = array("data" => $data);

        header('Content-Type: application/json');
        echo json_encode($array);
    }

    public function delete()
    {
        $data = $this->input->post();

        $sukses = $this->M_pembayaran->delete(base64_decode($data['id']));

        $result = [
            "errorId" => $sukses['success'] ? 0 : 1,
            "message" => $sukses['success'] ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        header('Content-type: application/json');
        echo json_encode($result);   
    }

    public function get_jenis_pembayaran()
    {
        $search = $this->input->get('q');

        $data = $this->M_pembayaran->get_jenis_pembayaran($search);

        //header("Content-type: application/json");
        header('Access-Control-Allow-Origin: *');
        echo json_encode($data);
    }

}

/* End of file Pembayaran.php */
/* Location: ./application/controllers/Pembayaran.php */