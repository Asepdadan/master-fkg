<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kelompok extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_datatables_data()
    {
        $query = "SELECT a.*,b.nama_periode,c.nama_semester
            FROM mst_kelompok a left join mst_periode b on a.id_periode = b.id
            left join mst_semester c on c.id = a.id_semester order by a.id desc";

        return $this->db->query($query)->result_array();
    }

    public function get_edit( $id )
    {
        $query = "SELECT a.*,b.nama_periode,c.nama_semester
            FROM mst_kelompok a left join mst_periode b on a.id_periode = b.id
            left join mst_semester c on c.id = a.id_semester
            WHERE a.id = ? ";

        return $this->db->query( $query, [$id] )->row_array();
    }


    public function create($data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->insert('mst_kelompok', $data);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }


    public function update( $id, $data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->update('mst_kelompok', $data,[ 'id' => $id ]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }

    public function delete($id)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->delete('mst_kelompok',['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }

    public function get_select2_kelompok($search)
    {
        $query = "select id,nama from mst_kelompok where nama like '%{$search}%'";

        return $this->db->query($query)->result_array();
    }

}

/* End of file M_periode.php */
/* Location: ./application/models/M_periode.php */