<link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css">

<script src="<?= base_url(); ?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<script src="<?= base_url();?>assets/global/scripts/handlebars.js" type="text/javascript"></script>


<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?= $title; ?></span>
        </div>
    </div>
    <div class="portlet-body">
        <!-- <div class="table-responsive"> -->
            <table class="table table-hover table-responsive table-bordered" id="tabel-mahasiswa">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>NIM</th>
                        <th>NAMA MAHASISWA</th>
                        <th>STATUS</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        <!-- </div> -->
        <div class="row margin-top-20">
            <a href="<?= base_url('akademik/add'); ?>" class="btn btn-primary">Tambah</a>
        </div>
    </div>
</div>

<script>
var tabel = "";
$(function () {
    tabel = $('#tabel-mahasiswa').DataTable({
        "processing": true,
        "ajax": "<?= base_url('mahasiswa/get_datatables_data'); ?>",
        "deferRender": true,
        "aLengthMenu": [[5, 10, 50, -1],[ 5, 10, 50, "All"]],
        "columns": [
            { "data": "id" },
            { "data": "nim" },
            { "data": "nama" },
            { "data": "nama" },
            { "render": function ( data, type, row ) {
                var html  = "<button class='btn btn-warning btnedit' data-id='"+ row.id +"' type='button'><i class='fa fa-edit' onclick='edit_data(\""+ row.id +"\")'></i></button> "
                    html += " <button data-row='"+ row.id +"' class='btn btn-danger btn-hapus-row' type='button' id='hapus'><i class='fa fa-trash'></i></button>"

                return html
                },
                "targets": 6
            },
        ],
    });

});

$(document).ready(function() {
    
    $('#tabel-mahasiswa').on('click', '.btn-hapus-row', function(event) {
       var row = $(this).data("row")
       if (confirm('Are you sure you want to save this thing into the database?')) {
            hapus_row(row)
        } else {
            
        }
    });

    $("#tabel-mahasiswa").on('click', '.btnedit', function(event) {
        document.location = '<?php base_url(); ?>mahasiswa/add/'+$(this).data('id')+'/'
    });

 });


function hapus_row(row) {
    //alert(row)
    $.ajax({
        url: '<?= base_url('mahasiswa/delete'); ?>',
        type: 'POST',
        dataType: 'json',
        data: {id: btoa(row),'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
    })
    .done(function(response) {
        if(response.errorId == 0) {
            toastr.success(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)                
        } else {
            toastr.error(response.message)
            setTimeout(function(){
                tabel.ajax.reload()
            },1000)
        }
    })    
 }
</script>

