<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_akademik extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_datatables_data()
    {
        $query = "SELECT *
            FROM mst_semester order by id desc";

        return $this->db->query($query)->result_array();
    }

    public function get_edit( $id )
    {
        $query = "SELECT *
            FROM mst_semester
            WHERE id = ? ";

        return $this->db->query( $query, [$id] )->row_array();
    }


    public function create($data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->insert('mst_semester', $data);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }


    public function update( $id, $data)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->update('mst_semester', $data,[ 'id' => $id ]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success,
            'data' => $success ? $data : null
        ];
    }

    public function delete($id)
    {
        $error=0;   
        $this->db->trans_begin();

        $success = $this->db->delete('mst_semester',['id' => $id]);
        
        if ($this->db->trans_status() === FALSE && !$success)
            $error++;
        
        if($error > 0)
        {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return [
            'success' => $success
        ];
    }

    public function get_select2_semester($search)
    {
        $query = "select id,nama_semester from mst_semester where nama_semester like '%{$search}%'";

        return $this->db->query($query)->result_array();
    }

}

/* End of file M_periode.php */
/* Location: ./application/models/M_periode.php */