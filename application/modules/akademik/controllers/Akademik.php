<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akademik extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_akademik');
    }

    public function index()
    {
        $data['title'] = ucfirst('akademik');
        $data['menu'] = 'akademik';
        $this->render_page('index',$data);    
    }

    public function add( $id = "" )
    {
        $data['title'] = ucfirst('form akademik');
        $data['menu'] = 'akademik';
        $data['id'] = $id;

        $this->render_page('add',$data);    
    }

    public function get_datatables_data()
    {
        $data = $this->M_akademik->get_datatables_data();
        $array = array("data" => $data);

        header('Content-Type: application/json');
        echo json_encode($array);
    }

    public function get_edit( $id = "" )
    {
        $data = [];
        $this->load->model('matakuliah/M_matakuliah');

        if( ! empty($id) )
            $data['EDIT'] = $this->M_akademik->get_edit( $id );
        else
            $data['matakuliah'] = $this->M_matakuliah->get_data_matakuliah();

        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function save()
    {
        $data = $this->input->post();
        
        unset($data['ci_csrf_token']);        

        if(empty($data['id'])){

            $success = $this->M_akademik->create($data);
        }else{

            $success = $this->M_akademik->update($data['id'],$data);
        }

        $result = [
            "errorId" => $success['success'] ? 0 : 1,
            "message" => $success['success'] ? "Data berhasil disimpan" : "Data gagal disimpan",
        ];

        header("Content-type: application/json");
        echo json_encode($result);
    }

    public function delete()
    {
        $data = $this->input->post();

        $sukses = $this->M_akademik->delete(base64_decode($data['id']));

        $result = [
            "errorId" => $sukses['success'] ? 0 : 1,
            "message" => $sukses['success'] ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        header('Content-type: application/json');
        echo json_encode($result);   
    }

    public function get_select2_akademik()
    {
        $search = $this->input->get('q');

        $result = $this->M_akademik->get_select2_akademik($search);

        header('Content-type: application/json');
        echo json_encode($result);      
    }

    public function random()
    {
        $hari = ["senin","selasa","rabu","kamis","jumat"];
        $kelompok = [0,1,2,3,4];
        $matkul = ["orto"];
        $index = 0;
        $string = "";
        $array = [];
        $rand =  mt_rand($kelompok,5);
        print_r($rand);die;
        echo "<br>";
        foreach ($hari as $value) {
            $rand =  array_rand($kelompok);

            $string .= $rand;
            array_push($array, [
                "hari" => $hari[$index],
                "kelompok" => $rand,
            ]);

            $index++;
        }
        echo "<pre>";
        print_r($array);
        echo "</pre>";

    }

}