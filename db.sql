/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.31-MariaDB : Database - project_fdg
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`project_fdg` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `project_fdg`;

/*Table structure for table `akd_jadwal` */

DROP TABLE IF EXISTS `akd_jadwal`;

CREATE TABLE `akd_jadwal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `id_matakuliah` int(11) NOT NULL,
  `id_dpjp` int(11) NOT NULL,
  `tahun_angkatan` varchar(10) NOT NULL,
  `kelompok` int(11) NOT NULL,
  `sub_kelompok` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `jenis_putaran` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Lanjut, 2=Ngulang',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `akd_jadwal` */

/*Table structure for table `akd_pengajuan` */

DROP TABLE IF EXISTS `akd_pengajuan`;

CREATE TABLE `akd_pengajuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_matakuliah` int(11) DEFAULT NULL,
  `nama_kalab` varchar(100) DEFAULT NULL,
  `id_mahasiswa` int(11) DEFAULT NULL,
  `nama_lokasi` varchar(200) DEFAULT NULL,
  `hari_kerja` varchar(50) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL COMMENT '2 minggu dari tanggal mulai',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `akd_pengajuan` */

insert  into `akd_pengajuan`(`id`,`id_matakuliah`,`nama_kalab`,`id_mahasiswa`,`nama_lokasi`,`hari_kerja`,`tanggal_mulai`,`tanggal_akhir`,`created_date`,`update_date`,`created_by`,`update_by`) values (1,2,'asep dadan',1,'rs hasan sadikin bandung','senin,selasa , rabu, kamis','2018-03-27','2018-04-30','2018-04-23 10:28:10','2018-04-23 10:28:10',NULL,NULL);

/*Table structure for table `akd_periode` */

DROP TABLE IF EXISTS `akd_periode`;

CREATE TABLE `akd_periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_mulai` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `periode` enum('1','2') DEFAULT NULL,
  `tahun_angkatan` varchar(10) DEFAULT NULL,
  `id_master_matakuliah` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `akd_periode` */

insert  into `akd_periode`(`id`,`tanggal_mulai`,`tanggal_akhir`,`periode`,`tahun_angkatan`,`id_master_matakuliah`) values (5,'2018-04-02','2018-04-30','2','2017/2018',2);

/*Table structure for table `bm_ketarampilan` */

DROP TABLE IF EXISTS `bm_ketarampilan`;

CREATE TABLE `bm_ketarampilan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_keterampilan` varchar(100) NOT NULL,
  `nilai` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `tanggal_mulai` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_selesai` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bm_ketarampilan` */

/*Table structure for table `bm_lembar_khusus` */

DROP TABLE IF EXISTS `bm_lembar_khusus`;

CREATE TABLE `bm_lembar_khusus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `nama_pencatatat` varchar(255) NOT NULL,
  `id_dpjp` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bm_lembar_khusus` */

/*Table structure for table `bm_nilai` */

DROP TABLE IF EXISTS `bm_nilai`;

CREATE TABLE `bm_nilai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_keterampilan` int(11) NOT NULL,
  `nilai_makalah` int(11) NOT NULL,
  `nilai_u_j` int(11) NOT NULL,
  `nilai_akhir` int(11) NOT NULL,
  `tanggal_mulai` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_selesai` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bm_nilai` */

/*Table structure for table `mst_dpjp` */

DROP TABLE IF EXISTS `mst_dpjp`;

CREATE TABLE `mst_dpjp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Laki-Laki, 2=Perempuan',
  `kewarganegaraan` tinyint(1) DEFAULT '1' COMMENT '1=WNI, 2=WNA',
  `id_kelurahan` int(11) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `rt` varchar(10) DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `agama` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Islam, 2=Kristen, 3=Katholik, 4=Hindu, 5=Budha, 6=Konghucu, 7=Lain-Lain',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Belum Nikah, 2=Nikah, 3=Janda, 4=Duda',
  `jenis_dpjp` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Umum, 2=Khusus',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=Dihapus, 0=Tidak Dihapus',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mst_dpjp` */

insert  into `mst_dpjp`(`id`,`nama`,`tempat_lahir`,`tanggal_lahir`,`jenis_kelamin`,`kewarganegaraan`,`id_kelurahan`,`alamat`,`rt`,`rw`,`agama`,`status`,`jenis_dpjp`,`create_date`,`update_date`,`create_user_id`,`update_user_id`,`is_delete`) values (1,'asep dadan','garut','2012-12-07',1,1,1,'garut jawa barat','07','03',1,1,1,'2018-04-18 06:39:31','0000-00-00 00:00:00',0,0,0),(2,'khanza fauziah','garut','2018-04-11',2,1,1,'garut jawa barat','07','03',1,1,1,'2018-04-18 06:39:10','0000-00-00 00:00:00',0,0,0);

/*Table structure for table `mst_jenis_pembayaran` */

DROP TABLE IF EXISTS `mst_jenis_pembayaran`;

CREATE TABLE `mst_jenis_pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `mst_jenis_pembayaran` */

insert  into `mst_jenis_pembayaran`(`id`,`nama_jenis`) values (4,'her registrasi'),(9,'pendidikan dasar'),(10,'senat'),(11,'paket ujian');

/*Table structure for table `mst_kelompok` */

DROP TABLE IF EXISTS `mst_kelompok`;

CREATE TABLE `mst_kelompok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `id_periode` int(11) DEFAULT NULL,
  `id_semester` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_kelompok` */

insert  into `mst_kelompok`(`id`,`nama`,`id_periode`,`id_semester`,`date_created`) values (1,'nama edited',4,1,'2018-04-18 07:33:33');

/*Table structure for table `mst_level` */

DROP TABLE IF EXISTS `mst_level`;

CREATE TABLE `mst_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `mst_level` */

insert  into `mst_level`(`id`,`nama_level`) values (1,'admin'),(2,'akademik'),(3,'staff akademik'),(4,'keuangan'),(5,'staff keuangan'),(6,'dpjp'),(7,'mahasiswa'),(8,'suster');

/*Table structure for table `mst_mahasiswa` */

DROP TABLE IF EXISTS `mst_mahasiswa`;

CREATE TABLE `mst_mahasiswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(20) NOT NULL,
  `nirm` varchar(20) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` tinyint(1) NOT NULL COMMENT '1=Laki-Laki, 2=Perempuan',
  `kewarganegaraan` tinyint(1) NOT NULL COMMENT '1=WNI, 2=WNA',
  `alamat` varchar(255) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `rt` varchar(10) DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `agama` tinyint(1) NOT NULL COMMENT '1=Islam, 2=Kristen, 3=Katholik, 4=Hindu, 5=Budha, 6=Konghucu, 7=Lain-Lain',
  `status` tinyint(1) NOT NULL COMMENT '1=Belum Nikah, 2=Nikah, 3=Janda, 4=Duda',
  `gelar` varchar(255) NOT NULL,
  `tahun_lulus` year(4) NOT NULL,
  `tanggal_daftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `semester` int(11) DEFAULT NULL,
  `putaran` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '0=Tidak Dihapus, 1=Dihapus',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nim` (`nim`),
  UNIQUE KEY `nirm` (`nirm`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_mahasiswa` */

insert  into `mst_mahasiswa`(`id`,`nim`,`nirm`,`nama`,`tempat_lahir`,`tanggal_lahir`,`jenis_kelamin`,`kewarganegaraan`,`alamat`,`id_kelurahan`,`rt`,`rw`,`agama`,`status`,`gelar`,`tahun_lulus`,`tanggal_daftar`,`semester`,`putaran`,`create_date`,`update_date`,`create_user_id`,`update_user_id`,`is_delete`) values (1,'13402351','2018-16-001','asep dadan','garut','2018-04-06',1,0,'garut',0,'01','01',0,0,'s.tr.kom',2017,'2018-04-20 20:57:04',1,4,'2018-04-20 20:57:04',NULL,0,NULL,0);

/*Table structure for table `mst_matakuliah` */

DROP TABLE IF EXISTS `mst_matakuliah`;

CREATE TABLE `mst_matakuliah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `sks` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0= tidak wajib 1: wajib',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Tidak Dihapus, 1=Dihapus',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `mst_matakuliah` */

insert  into `mst_matakuliah`(`id`,`kode`,`nama`,`sks`,`status`,`is_delete`) values (2,'orto','orto',2,1,0),(3,'IMTK','ILMU MATERIAL TEKNOLOGI KEDOKTERAN GIGI 2	',4,0,0),(4,'biomedi','biomedik',2,0,0),(5,'IKGM','ikmg kepaniteraan',2,0,0),(6,'EPIDEMOLOGI ','epidemologi dan pencegahan kg',2,0,0),(7,'SI','sistem informasi',4,0,0),(8,'komputer','komputer',2,0,0);

/*Table structure for table `mst_pembayaran` */

DROP TABLE IF EXISTS `mst_pembayaran`;

CREATE TABLE `mst_pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_pembayaran` int(11) NOT NULL,
  `tahun_angkatan` varchar(20) NOT NULL,
  `harga` float(15,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `mst_pembayaran` */

insert  into `mst_pembayaran`(`id`,`id_jenis_pembayaran`,`tahun_angkatan`,`harga`) values (1,3,'2010',12000000),(2,3,'2018',29000000),(3,4,'2011',120),(4,4,'2016',900000000),(5,10,'2012',80000000);

/*Table structure for table `mst_pembayaran_sks` */

DROP TABLE IF EXISTS `mst_pembayaran_sks`;

CREATE TABLE `mst_pembayaran_sks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_angkatan` varchar(10) DEFAULT NULL,
  `nominal` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_pembayaran_sks` */

/*Table structure for table `mst_ruangan` */

DROP TABLE IF EXISTS `mst_ruangan`;

CREATE TABLE `mst_ruangan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_dpjp` int(11) NOT NULL,
  `suster` int(11) DEFAULT NULL COMMENT 'mengambil id user dg level suster',
  `nama_ruangan` varchar(200) NOT NULL,
  `jenis_ruangan` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Umum, 2=Khusus',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_ruangan` */

insert  into `mst_ruangan`(`id`,`id_dpjp`,`suster`,`nama_ruangan`,`jenis_ruangan`) values (1,1,NULL,'ruangan',0);

/*Table structure for table `mst_semester` */

DROP TABLE IF EXISTS `mst_semester`;

CREATE TABLE `mst_semester` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_semester` varchar(10) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_semester` */

insert  into `mst_semester`(`id`,`nama_semester`,`jumlah`) values (1,'nama semes',12);

/*Table structure for table `mst_sub_kelompok` */

DROP TABLE IF EXISTS `mst_sub_kelompok`;

CREATE TABLE `mst_sub_kelompok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelompok` int(11) DEFAULT NULL,
  `id_mahasiswa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_sub_kelompok` */

/*Table structure for table `mst_users` */

DROP TABLE IF EXISTS `mst_users`;

CREATE TABLE `mst_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Admin, 2= Mahasiswa, 3=Dokter/DPJP, 4=Akademik, 5=Keuangan',
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `id_mahasiswa` int(11) DEFAULT NULL,
  `id_dpjp` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Tidak Dihapus, 1=Dihapus',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_users` */

/*Table structure for table `pm_lembar_khusus` */

DROP TABLE IF EXISTS `pm_lembar_khusus`;

CREATE TABLE `pm_lembar_khusus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `catatan` varchar(255) NOT NULL,
  `nama_pencatat` varchar(255) NOT NULL,
  `id_dpjp` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `pm_lembar_khusus` */

insert  into `pm_lembar_khusus`(`id`,`tanggal`,`catatan`,`nama_pencatat`,`id_dpjp`,`id_mahasiswa`) values (1,'2018-04-18 07:04:20','catatan','asdan',2,5);

/*Table structure for table `pm_nilai_akhir_klinik` */

DROP TABLE IF EXISTS `pm_nilai_akhir_klinik`;

CREATE TABLE `pm_nilai_akhir_klinik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_akhir_klinik` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_dpjp` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pm_nilai_akhir_klinik` */

/*Table structure for table `pm_nilai_diskusi` */

DROP TABLE IF EXISTS `pm_nilai_diskusi`;

CREATE TABLE `pm_nilai_diskusi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_diskusi` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_dpjp` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pm_nilai_diskusi` */

/*Table structure for table `pm_nilai_makalah` */

DROP TABLE IF EXISTS `pm_nilai_makalah`;

CREATE TABLE `pm_nilai_makalah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `nilai_makalah` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_dpjp` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pm_nilai_makalah` */

/*Table structure for table `pm_nilai_status` */

DROP TABLE IF EXISTS `pm_nilai_status`;

CREATE TABLE `pm_nilai_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_status` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_dpjp` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pm_nilai_status` */

/*Table structure for table `pm_nilai_ujian` */

DROP TABLE IF EXISTS `pm_nilai_ujian`;

CREATE TABLE `pm_nilai_ujian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_pm_nilai_diskusi` int(11) NOT NULL,
  `id_pm_nilai_makalah` int(11) NOT NULL,
  `id_pm_nilai_status` int(11) NOT NULL,
  `id_pm_nilai_akhir_klinik` int(11) NOT NULL,
  `nilai_ujian_komprehensif` int(11) NOT NULL,
  `nilai_akhir_profesi` int(11) NOT NULL,
  `id_dpjp` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pm_nilai_ujian` */

/*Table structure for table `uang_bayar` */

DROP TABLE IF EXISTS `uang_bayar`;

CREATE TABLE `uang_bayar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `no_rekening` varchar(255) NOT NULL,
  `nama_rekening` varchar(255) NOT NULL,
  `metode_pembayaran` varchar(255) NOT NULL,
  `nominal_transfer` decimal(10,0) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL COMMENT '1=Menunggu Bayar, 2=Lunas, 3=Batal, 4=Gagal',
  `keteranagn` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `uang_bayar` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
